# Spring Data MongoDB Demo

This application demonstrates developing an application that uses `spring-data-mongodb` to simplify interactions with MongoDB. 

As with all SpringData implementations, you can get CRUD functionality up and running with 4 simple steps.

1. Include the dependency in your pom.xml (Assumes this is a Spring Boot Application)
```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
        </dependency>
```
2. Add the `@EnableMongoRepositories` annotation to your Main application class. This will tell SpringBoot to look for repository classes.
```java
@SpringBootApplication
@EnableMongoRepositories
public class MongoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoDemoApplication.class, args);
    }
}
```

3. Extend one of the MongoDB Repository interfaces, specifying the data object that maps to the MongoDB repository, and the key data type. This provides significant default behavior. Note that you do not need to define any interface methods as the default behavior includes the standard CRUD operations as well as the ability to provide Pageable and Sort objects to influence the results of the queries.
```java
public interface CustomerRepository extends MongoRepository<Customer, String> {

}
```
4. Create the database connection information in application.yml or application.properties.
```yaml
spring:
  data:
    mongodb:
      database: ${MONGO_DATABASE:testdb}
      authentication-database: ${MONGO_AUTH_DATABASE:admin}
      username: ${MONGO_USERNAME:root}
      password: ${MONGO_PASSWORD:p4ssw0rd}
      port: ${MONGO_PORT:27017}
      host: ${MONGO_HOST:localhost}
```
Note: The above yaml snippet shows how you can use environment variables to override these properties while still providing meaningful default values.



## Running the Application Locally
The application includes a REST controller which manages CRUD operations for a Customer entity. There is no "business-logic" in this demo application, the REST Controller simply passes the requests to the Repository to interact with the Mongo Database.

### Docker ###
The root of the project includes a `docker-compose.yml` file for local testing with docker.
The `docker-compose.yml` will create `mongodb` and `mongo-express` containers and mount a local volume so that your data will be persisted across re-starts.
`mongo-express` provides a WebUI for working with the mongodb.

You can run the application locally by executing the following commands from the root directory.
```shell script
docker-compose up -d
```
and then executing the spring-boot maven target
```shell script
mvn spring-boot:run
```

Once the application is running, you can use the `/customers` endpoint for CRUD operations on the database. 

You can then read all customers using the following curl command.
``` shell script
curl http://localhost:8080/customers
```
And write a new customer using this curl command
```shell script
curl --header "Content-Type: application/json" \
     --request POST \
     --data '{"firstName": "Test","lastName": "Customer","addresses": [{"address1": "456 Main Street","city": "New York","state": "NY","zip": "10002"}]}' \
     http://localhost:8080/customers
```



## Deploying the application and a MongoDB to Kubernetes(K8s) ###
the ./k8s folder contains the following kubernetes artifacts.
1. `01-mongodb-secret.yaml` - contains a K8s `Secret` containing the username/password of the MongoDB server in encrypted form. 
1. `02-mongodb-config.yaml` - contains a K8s `ConfigMap` with environment variables identifying the MongoDB server. (minus security info)
1. `03-mongodb-statefulset.yaml` - contains the `StatefulSet` definition of the MongoDB.
1. `04-mongodb-demo.yaml` - contains the `Deployment` of the `mongodb-demo` application.**

None of the files have a namespace specified so unless you're deploying them to the default namespace, you need to either specify the namespace when deploying the artifact using `kubectl -n <namespace> apply -f <filename>`, 
or execute `kubectl config set-context --current --namespace <namesapce>` prior to deploying any of the files using `kubectl apply -f <filename>`.

The `04-mongodb-demo.yaml` references generic image, `spring-data-mongo-demo:latest` and will need to be updated with the location of the local image repository. For example `harbor.pksbeachhouse.com/library/spring-data-mongo-demo:latest`. 
The deployment also includes a reference to `harbor-creds`, which is a `docker-registry-secret` containing the login information for the harbor instance this image will be pulled from.
```yaml
    spec:
      imagePullSecrets:
        - name: harbor-creds
      containers:
      - name: mongodb-demo
```
The creation of this k8s secret is a one-time operation for the namespace which is why it's not included in this repo. It's created using the following `kubectl` command.
```shell script
  kubectl create secret docker-registry harbor-creds --docker-username=[username] --docker-password='[password]' --docker-email=someone@gmail.
```


Both the Demo application and the MongoDB instance take the username/password from the mongodb-secret generated from the `mongodb-secret.yaml` file. You can re-create this file with different values for username/password using the below command.
```shell script
kubectl create secret generic mongodb-secret --from-literal=username=<some-username> --from-literal=password=<some-password> \
        -o yaml --dry-run=client > 01-mongodb-secret.yaml 
```


If the k8s cluster does not have a configured LoadBalancer allowing you to expose an external IP Address, you can use the below command in order to test out the application.
```shell script
kubectl port-forward deployment/mongodb-demo 8080:8080
```

## Notes on Testing ##
This application demonstrates a couple of different patterns for testing REST Controllers and Repositories. 

It's considered a best-practice to limit the Spring configuration to only what is required. For example, even though this test is an integration test, testing from the controller through to the database, it doesn't use the `@SpringBootTest` annotation.  This test instead uses the `@DataMongoTest` annotation which will create a local mongodb for the test and use the MongoTemplate to load test data from a file resource.
It's also generally unnecessary to test functionality for code that implemented by the SpringData library.

```java
@DataMongoTest
class CustomerControllerIT {

    @Autowired
    private CustomerRepository customerRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private MockMvc mvc;

    @BeforeEach
    public void initializeClient(@Autowired MongoTemplate mongoTemplate, @Value("classpath:test-customers.json") Resource testCustomersResource) throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new CustomerController(customerRepository)).build();

        mongoTemplate.dropCollection("customer");

        Customer[] testCustomers = objectMapper.readValue(testCustomersResource.getInputStream().readAllBytes(), Customer[].class);
        Arrays.stream(testCustomers).sequential().forEach(mongoTemplate::insert);

    }
}
```
This code above demonstrates loading test data from files on the filesystem prior to running the tests, however this can also be done in an individual test as shown below.
```java
    @Test
    void addCustomer(@Value("classpath:test-customer.json") Resource testCustomerResource) throws Exception {

        mvc.perform(post("/customers")
            .content(testCustomerResource.getInputStream().readAllBytes())
            .accept(MediaType.APPLICATION_JSON) //Response Type
            .contentType(MediaType.APPLICATION_JSON)) //Request Type
            .andExpect(status().isOk())
            .andDo(print()) //prints out request/response information. Useful in debugging.
            .andExpect(jsonPath("$.firstName", is("Test")))
            .andExpect(jsonPath("$.lastName", is("Customer")));
    }

```
 
You can find links to additional information in the [Help](./HELP.md) document.
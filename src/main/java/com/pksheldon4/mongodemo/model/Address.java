package com.pksheldon4.mongodemo.model;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
public class Address {

    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;

}

package com.pksheldon4.mongodemo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class Customer {

    @Id
    private String customerId;

    @Indexed
    private String firstName;
    private String lastName;

    private List<Address> addresses;

    public Customer() {

    }
}

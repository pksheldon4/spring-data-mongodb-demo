package com.pksheldon4.mongodemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
@EnableMongoAuditing
@Slf4j
public class MongoDemoApplication {

    public static void main(String[] args) {
        log.info("##########################################################################################");
        log.info("##########################################################################################");
        log.info("############################### Mongo Demo Application ###################################");
        log.info("##########################################################################################");
        log.info("##########################################################################################");
        SpringApplication.run(MongoDemoApplication.class, args);
    }

}

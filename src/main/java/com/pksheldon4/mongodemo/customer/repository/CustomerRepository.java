package com.pksheldon4.mongodemo.customer.repository;

import com.pksheldon4.mongodemo.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    List<Customer> findAllByLastName(String lastName);

    List<Customer> findAllByAddresses_Zip(String zip);
}

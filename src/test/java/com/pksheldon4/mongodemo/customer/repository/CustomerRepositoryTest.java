package com.pksheldon4.mongodemo.customer.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pksheldon4.mongodemo.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@DataMongoTest
public class CustomerRepositoryTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Value("classpath:test-customers.json")
    Resource testData;
    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    public void setUp(@Autowired MongoTemplate mongoTemplate) throws Exception {

        mongoTemplate.dropCollection("customer");
        Customer[] customers = objectMapper.readValue(testData.getInputStream().readAllBytes(), Customer[].class);
        Arrays.stream(customers).sequential().forEach(mongoTemplate::insert);
    }

    @Test
    public void findAllByLastName_returnsCustomer() {
        List<Customer> customers = customerRepository.findAllByLastName("Sheldon");
        assertThat(customers).isNotNull();
        assertThat(customers.size()).isEqualTo(1);
        Customer customer = customers.get(0);
        assertThat(customer.getFirstName()).isEqualTo("Preston");
        assertThat(customer.getAddresses().size()).isEqualTo(2);
    }

    @Test
    void findAllByAddresses_Zip_returnsCustomer() {
        List<Customer> customers = customerRepository.findAllByAddresses_Zip("10001");
        assertThat(customers).isNotNull();
        assertThat(customers.size()).isEqualTo(1);
        Customer customer = customers.get(0);
        assertThat(customer.getFirstName()).isEqualTo("Preston");
        assertThat(customer.getAddresses().size()).isEqualTo(2); //Still 2 since the entire customer is returned, not just the 1 address
    }
}
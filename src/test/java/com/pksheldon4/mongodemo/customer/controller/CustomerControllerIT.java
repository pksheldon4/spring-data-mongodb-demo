package com.pksheldon4.mongodemo.customer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pksheldon4.mongodemo.customer.repository.CustomerRepository;
import com.pksheldon4.mongodemo.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This test manually configures the Controller in the `initializeClient` method so the only Spring auto-configuration that's
 * required is the CustomerRepository.
 */
@DataMongoTest
class CustomerControllerIT {

    @Autowired
    private CustomerRepository customerRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private MockMvc mvc;

    @BeforeEach
    public void initializeClient(@Autowired MongoTemplate mongoTemplate, @Value("classpath:test-customers.json") Resource testCustomersResource) throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new CustomerController(customerRepository)).build();

        mongoTemplate.dropCollection("customer");

        Customer[] testCustomers = objectMapper.readValue(testCustomersResource.getInputStream().readAllBytes(), Customer[].class);
        Arrays.stream(testCustomers).sequential().forEach(mongoTemplate::insert);

    }

    @Test
    void addCustomer(@Value("classpath:test-customer.json") Resource testCustomerResource) throws Exception {

        mvc.perform(post("/customers")
            .content(testCustomerResource.getInputStream().readAllBytes())
            .accept(MediaType.APPLICATION_JSON) //Response Type
            .contentType(MediaType.APPLICATION_JSON)) //Request Type
            .andExpect(status().isOk())
            .andDo(print()) //prints out request/response information. Useful in debugging.
            .andExpect(jsonPath("$.firstName", is("Test")))
            .andExpect(jsonPath("$.lastName", is("Customer")));
    }

    @Test
    void getCustomers() throws Exception {
        mvc.perform(get("/customers")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].customerId", is("1")))
            .andExpect(jsonPath("$[0].firstName", is("Preston")));
    }

    @Test
    void getCustomerById() throws Exception {
        mvc.perform(get("/customers/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.firstName", is("Preston")));
    }

    @Test
    void deleteCustomerById() throws Exception {
        mvc.perform(delete("/customers/" + UUID.randomUUID().toString())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}